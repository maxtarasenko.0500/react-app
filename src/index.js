import React from 'react';
import ReactDOM from 'react-dom/client';
import { useState } from 'react';
import './index.css';

const planetList = (
  <ul className='planets-list'>
    <li>Mercury</li>
    <li>Venus</li>
    <li>Earth</li>
    <li>Mars</li>
    <li>Jupiter</li>
    <li>Saturn</li>
    <li>Uranus</li>
    <li>Neptune</li>
  </ul>
)

const Toggle = () => {
  const [isActive, setIsActive] = useState(false);
  const handleClick = () => {
    if (!isActive) {
      document.body.classList.add('dark');
      document.querySelector('.planets-list').classList.add('dark')
    } else {
      document.body.classList.remove('dark')
      document.querySelector('.planets-list').classList.remove('dark')
    }
    setIsActive(current => !current);
  };
  return (
    <label className="switch" htmlFor="checkbox">
      <input type="checkbox" onClick={handleClick} id="checkbox" />
      <div className="slider round"></div>
    </label>
  )
}

const root = ReactDOM.createRoot(document.getElementById('root'))
root.render(
  [React.createElement("h1", { style: { color: '#999', fontSize: '19px' } }, "Solar system planets"), planetList, <Toggle></Toggle>]
);  
